{--ctype Mystery where
  El : Mystery
  Cons : Mystery -> Mystery
--}


ctype Nat where
  Z : Nat
  S : Nat -> Nat

plus: (Nat, Nat)->Nat
plus. (Z, y) = y 
plus. (S.x, y)=  S.(plus.(x,y))

ctype Lst where
  El : Lst
  Cons : Int -> Lst ->Lst

app:(Lst, Lst)->Lst
app.(El, y) = y
app.(Cons.x.l, y)= Cons.x.a
                  where
                    a = app.(l, y)
