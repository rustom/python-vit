from enum import Enum
class Lsttag(Enum):
    El  =1
    Cons=2
    def __repr__(self):
       return (self.name)

El = Lsttag.El
Cons = Lsttag.Cons

#app:(Lst, Lst)->Lst
#app.(El, y) = y
#app.(Cons.x.l, y)= Cons.x.a
#                  where
#                    a = app.(l, y)


def app(z, y):
    if z == El:
        return y
    else:
        (c,x,l) = z
        return (Cons, x, (app(l,y)))
