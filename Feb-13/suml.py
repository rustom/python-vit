# Build and return a list
def firstn1(n):
    num, nums = 0, []
    while num < n:
        nums.append(num)
        num += 1
    return nums

def firstng(n):
    num = 0 
    while num < n:
        #nums.append(num)
        yield num
        num += 1
 #   return nums

def firstn2(n):
    num, sum = 0, 0
    while num < n:
        sum +=num
        num += 1
    return sum

def makegen(l):
      for x in l:
          print(x)

def g():
    yield 1
    yield 2
    yield 3


#for (i=0; i<N; i++)
#  sum += a[i]

#for x in a:
#    sum += x
















# a generator that yields items instead of returning a list
def firstngen(n):
    num = 0
    while num < n:
        yield num
        num += 1

# Using the generator pattern (an iterable)
class firstn(object):
    def __init__(self, n):
        self.n = n
        self.num, self.nums = 0, []

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    def next(self):
        if self.num < self.n:
            cur, self.num = self.num, self.num+1
            return cur
        else:
            raise StopIteration()

