from os.path import isdir, isfile
from os import chdir, listdir
# this seems to work
def filewalk(p):
    print ("p", p)
    for f in listdir(p):
        yield f
        print ("f %s isdir %d" % (f, isdir(f)))
        if isdir(f):
       #     yield from filewalk(f)
             for c in filewalk(f): yield c

            
def fw2(p):
    if isfile(p):
        print ('Simple ', p)
        yield p
    else:
        for f in listdir(p):
            print ("f ", f)
            yield f
            for c in fw2(f):
                print ("c ", c)
                yield c


def fw3(p):
    if isfile(p):
        return [p]
    else:
        ls = [p]
        for f in listdir(p):
           # print ("f ", f, ls)
            ls = ls+[f]
            for c in fw3(p+'/'+f):
              #  print ("c ", c, ls)
                ls = ls+[c]

        return ls

        # return [c for f in listdir(p) for c in fw3(f)]

def fw4(p):
    if isfile(p):
        return [p]
    else:
        return [p] + [c for f in listdir(p) for c in fw4(p+'/'+f)]


def fw5(p):
    yield p
    if isdir(p):
        yield from (c for f in listdir(p) for c in fw5(p+'/'+f))


def qsort(l):
    if l==[]:
        return []
    else:
        f,r = l[0], l[1:]
        return qsort([x for x in r if x < f]) + [f] + qsort([x for x in r if x >= f])
