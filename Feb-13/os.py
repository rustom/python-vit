from os.path import isdir, isfile
from os import chdir, listdir

def filewalk(p):
    for f in listdir(p):
        yield f
        print ("f %s isdir %d" % (f, isdir(f)))
        if isdir(f):
       #     yield from filewalk(f)
             for c in filewalk(f): yield c
            
def fw2(p):
    if isfile(p):
        print ('Simple ', p)
        yield p
    else:
        for f in listdir(p):
            print ("f ", f)
            yield f
            for c in fw2(f):
                print ("c ", c)
                yield c


def fw3(p):
    if isfile(p):
        return [p]
    else:
        ls = [p]
        for f in listdir(p):
            print ("f ", f, ls)
            ls = ls+[f]
            for c in fw3(f):
                print ("c ", c, ls)
                ls = ls+[c]

        return ls

        # return [c for f in listdir(p) for c in fw3(f)]
