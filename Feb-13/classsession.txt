Python 3.4.2 (default, Oct  8 2014, 13:08:17) 
[GCC 4.9.1] on linux
Type "copyright", "credits" or "license()" for more information.
>>> ================================ RESTART ================================
>>> 
>>> sum(range(50000000))
1249999975000000
>>> l = [1,2]
>>> l.append(3)
>>> l
[1, 2, 3]
>>> l.append(3)
>>> l
[1, 2, 3, 3]
>>> range(10)
range(0, 10)
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> sum(range(10))
45
>>> sum(firstn1(10))
45
>>> ================================ RESTART ================================
>>> 
>>> firstn2(5)
10
>>> sum(firstn1(5))
10
>>> firstn2(50000000)
1249999975000000
>>> firstn2(80000000)
3199999960000000
>>> ================================ RESTART ================================
>>> 
>>> g()
1
>>> g()
1
>>> g()
1
>>> g()
1
>>> g()
1
>>> ================================ RESTART ================================
>>> 
>>> g()
<generator object g at 0x7ff05b379708>
>>> i = g()
>>> dir(i)
['__class__', '__del__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__iter__', '__le__', '__lt__', '__name__', '__ne__', '__new__', '__next__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'close', 'gi_code', 'gi_frame', 'gi_running', 'send', 'throw']
>>> i.next
Traceback (most recent call last):
  File "<pyshell#22>", line 1, in <module>
    i.next
AttributeError: 'generator' object has no attribute 'next'
>>> i.__next__
<method-wrapper '__next__' of generator object at 0x7ff05b37b438>
>>> i.__next__()
1
>>> i.__next__()
2
>>> i.__next__()
3
>>> i.__next__()
Traceback (most recent call last):
  File "<pyshell#27>", line 1, in <module>
    i.__next__()
StopIteration
>>> i
<generator object g at 0x7ff05b37b438>
>>> g
<function g at 0x7ff05b3788c8>
>>> i.__next__()
Traceback (most recent call last):
  File "<pyshell#30>", line 1, in <module>
    i.__next__()
StopIteration
>>> i.__next__()
Traceback (most recent call last):
  File "<pyshell#31>", line 1, in <module>
    i.__next__()
StopIteration
>>> i = g()
>>> next(i)
1
>>> next(i)
2
>>> next(i)
3
>>> next(i)
Traceback (most recent call last):
  File "<pyshell#36>", line 1, in <module>
    next(i)
StopIteration
>>> next(i)
Traceback (most recent call last):
  File "<pyshell#37>", line 1, in <module>
    next(i)
StopIteration
>>> i = g()
>>> for x in i:
	print (x)

	
1
2
3
>>> for x in i:
	print (x)

	
>>> next(i)
Traceback (most recent call last):
  File "<pyshell#44>", line 1, in <module>
    next(i)
StopIteration
>>> for x in i:
	print (x)

	
>>> for x in i:	print (x)

>>> i = g()
>>> for x in i:	print (x)

1
2
3
>>> for x in g(): print (x)

1
2
3
>>> for x in g(): print (x)

1
2
3
>>> for x in g(): print (x)

1
2
3
>>> for x in g(): print (x)

1
2
3
>>> i = g()
>>> for x in i:	print (x)

1
2
3
>>> for x in i:	print (x)

>>> for x in i:	print (x)

>>> 
>>> [x*x for x in range(1,10)]
[1, 4, 9, 16, 25, 36, 49, 64, 81]
>>> [x*x for x in g()]
[1, 4, 9]
>>> 2 in [1,2,3,4]
True
>>> 22 in [1,2,3,4]
False
>>> 22 in g()
False
>>> 2 in g()
True
>>> for x in [1,2,3]: print x
SyntaxError: Missing parentheses in call to 'print'
>>> for x in [1,2,3]: print(x)

1
2
3
>>> l=[1,2,3]
>>> next(l)
Traceback (most recent call last):
  File "<pyshell#78>", line 1, in <module>
    next(l)
TypeError: 'list' object is not an iterator
>>> dir(l)
['__add__', '__class__', '__contains__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__', '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__rmul__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
>>> l.__getitem
Traceback (most recent call last):
  File "<pyshell#80>", line 1, in <module>
    l.__getitem
AttributeError: 'list' object has no attribute '__getitem'
>>> l.__getitem__
<built-in method __getitem__ of list object at 0x7ff05b38b288>
>>> l.__getitem__(0)
1
>>> l.__getitem__(2)
3
>>> l.__getitem__(1)
2
>>> l.__len__
<method-wrapper '__len__' of list object at 0x7ff05b38b288>
>>> l.__len__()
3
>>> ================================ RESTART ================================
>>> 
Traceback (most recent call last):
  File "/home/rusi/python-vit/Feb-13/generators/suml.py", line 33, in <module>
    for x in a:
NameError: name 'a' is not defined
>>> ================================ RESTART ================================
>>> 
>>> ================================ RESTART ================================
>>> 
>>> firstng(4)
<generator object firstng at 0x7f2ef2a81750>
>>> g=firstng(4)
>>> next(g)
0
>>> next(g)
1
>>> next(g)
2
>>> next(g)
3
>>> next(g)
Traceback (most recent call last):
  File "<pyshell#93>", line 1, in <module>
    next(g)
StopIteration
>>> sum(g)
0
>>> sum(firsng(4))
Traceback (most recent call last):
  File "<pyshell#95>", line 1, in <module>
    sum(firsng(4))
NameError: name 'firsng' is not defined
>>> sum(firstng(4))
6
>>> list(firstng(4))
[0, 1, 2, 3]
>>> sum(firstng(50000000))
1249999975000000
>>> sum(firstng(100000000))
4999999950000000
>>> ================================ RESTART ================================
>>> 
>>> makegen([1,2,3])
<generator object makegen at 0x7fd8c7c91750>
>>> g=makegen([1,2,3])
>>> next(g)
1
>>> next(g)
2
>>> next(g)
3
>>> next(g)
Traceback (most recent call last):
  File "<pyshell#105>", line 1, in <module>
    next(g)
StopIteration
>>> ================================ RESTART ================================
>>> 
>>> g=makegen([1,2,3])
>>> next(g)
1
>>> next(g)
2
>>> next(g)
3
>>> next(g)
1
>>> next(g)
2
>>> next(g)
3
>>> ================================ RESTART ================================
>>> 
>>> makegen([1,2,3])
1
2
3
>>> g=makegen([1,2,3])
1
2
3
>>> next(g)
Traceback (most recent call last):
  File "<pyshell#115>", line 1, in <module>
    next(g)
TypeError: 'NoneType' object is not an iterator
>>> 
