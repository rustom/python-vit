from enum import Enum

class Trtag(Enum):
    br=1
    et=2
    def __repr__(self):
       return (self.name)

et = Trtag.et
br = Trtag.br

def dfs0(t):
    if t != et:
        (br,num,t1,t2) = t
        print(num)
        return [num]+dfs(t1)+dfs(t2)

def dfs1(t):
    if t == et:
        return []
    else:
        (br,num,t1,t2) = t
        return dfs1(t1)+[num]+dfs1(t2)
# dfs same as inorder
# preorder, same :return [num]+dfs1(t1)+dfs1(t2)
# postorder, same:return dfs1(t1)+dfs1(t2)+[num]

t2 =(br,4,(br,2, et, et),(br,7,et,et))
t5 =(br,3,t2,t2)
tl =(br,4,[et,et,(br,3,[et,(br,6,[et,et])])])
def dfs2(t):
    if t == et:
        return []
    else:
        (br,num,ts) = t
        l =[num]
        return [l.append(dfs2(x)) for x in ts]

def dfsg(t):
    if t!=et:
        (br,num, t1,t2) = t
        yield num
        for i in iter(dfsg(t1)):
           yield i
        for i in iter(dfsg(t2)):
           yield i

def dfsg1(t):
    if t!=et:
        (br,num, t1,t2) = t
        yield num
        yield from dfsg(t1)
        yield from dfsg(t2)
        
